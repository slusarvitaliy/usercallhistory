<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class User extends Model
{
    public function calls()
    {
        return $this->hasMany(Call::class);
    }
}
