<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index(){
        $userList = User::select(['id','name','lastname','email'])->get();
        //dd($userList);
        return view('users', compact('userList'));
    }
}
