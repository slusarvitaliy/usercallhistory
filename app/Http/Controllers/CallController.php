<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Call;
use App\User;

class CallController extends Controller
{
    public function history($id){
        $callInfo = Call::select(['id','description','status','created_at'])->where('user_id', $id)->orderBy('created_at', 'desc')->get();
        $userInfo = User::select(['name','lastname'])->where('id', $id)->first();

        foreach ($callInfo as $info){
            if($info->status == 1){
                array_add($info, 'status_name','Outgoing');
                array_add($info, 'class','table-warning');
            }else{
                array_add($info, 'status_name','Incoming');
                array_add($info, 'class','table-success');
            }
        }

        return view('history', compact('userInfo', 'callInfo'));
    }
}
