@extends('site')

@section('content')
    <h1>Call History: {{ $userInfo->name}} {{ $userInfo->lastname}}</h1>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">created</th>
            <th scope="col">description</th>
            <th scope="col">status</th>
        </tr>
        </thead>
        <tbody>

        @foreach($callInfo as $info)
        <tr class="{{$info->class}}">
            <td width="10%">{{$info->created_at}}</td>
            <td >{{$info->description}}</td>
            <td width="10%"><b>{{$info->status_name}}</b></td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <p><a href="/" class="btn btn-dark">  Back to user List</a></p>
@endsection

