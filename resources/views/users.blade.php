@extends('site')

@section('content')
    <h1>User List</h1>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">E-mail</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($userList as $user)
        <tr>
            <th scope="row">{{$user->id}}</th>
            <td>{{$user->name}}</td>
            <td>{{$user->lastname}}</td>
            <td>{{$user->email}}</td>
            <td width="50px"><a href="{{route('callHistory',['id'=>$user->id])}}" class="btn btn-info">History</a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection

